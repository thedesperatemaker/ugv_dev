/*
 * protocol_detector.hpp
 *
 * Created on: Jul 15, 2021 14:03
 * Description:
 *
 * Copyright (c) 2021 Weston Robot Pte. Ltd.
 */

#ifndef PROTOCOL_DETECTOR_HPP
#define PROTOCOL_DETECTOR_HPP

#include <atomic>

#include "ugv_sdk/details/async_port/async_can.hpp"
#include "ugv_sdk/details/interface/parser_interface.hpp"

#include "/home/docker_hunter/colcon_ws/src/ugv_dev/include/cannet_nt200/include/EciApi130.hpp"

namespace westonrobot {
class ProtocolDectctor {
 public:
  bool Connect(std::string can_name);

  ProtocolVersion DetectProtocolVersion(uint32_t timeout_sec);

 private:
  std::shared_ptr<AsyncCAN> can_;
  std::shared_ptr<EciApi130> eci_can_;

  void ParseCANFrame(can_frame *rx_frame);

  std::atomic<bool> msg_v1_detected_;
  std::atomic<bool> msg_v2_detected_;
};
}  // namespace westonrobot

#endif /* PROTOCOL_DETECTOR_HPP */
